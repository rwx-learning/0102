FROM golang:1.16 as builder-1.16
WORKDIR /code
ENV GO111MODULE=auto
RUN go get -d -v golang.org/x/net/html  
COPY app.go ./
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o app .

FROM alpine:latest
ARG VERSION=1.16
WORKDIR /root/
COPY --from=builder-${VERSION} /code/app ./
CMD ["./app"]
